#include "../T2MbtsFex.h"
#include "../T2MbtsHypo.h"
#include "../TrigCountSpacePoints.h"
#include "../TrigCountSpacePointsHypo.h"
#include "../TrigCountTrtHits.h"
#include "../TrigCountTrtHitsHypo.h"
#include "../T2ZdcFex.h"
#include "../T2ZdcHypo.h"
#include "../TrigCountSpacePointsMT.h"


DECLARE_COMPONENT( T2MbtsFex )
DECLARE_COMPONENT( T2MbtsHypo )
DECLARE_COMPONENT( TrigCountSpacePoints )
DECLARE_COMPONENT( TrigCountSpacePointsHypo )
DECLARE_COMPONENT( TrigCountTrtHits )
DECLARE_COMPONENT( TrigCountTrtHitsHypo )
DECLARE_COMPONENT( T2ZdcFex )
DECLARE_COMPONENT( T2ZdcHypo )
DECLARE_COMPONENT( TrigCountSpacePointsMT )
